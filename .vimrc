" VIM DEFAULT COMMANDS

" MY PERSONAL COMMANDS

" key mapping
" switch tabs
noremap <F7> :tabp<CR>
noremap <F8> :tabn<CR>

" set tab length
set shiftwidth=4
set tabstop=4
set expandtab

" set backup directory
set backup
set backupdir=/home/william/backups

" set current working directory of window to that of the file being edited
autocmd BufEnter * silent! lcd %:p:h

" for Vim-LaTeX
" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a single file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" for editing of Matlab files
source $VIMRUNTIME/macros/matchit.vim
autocmd BufEnter *.m compiler mlint

" maximum line length 80char and break between words
set textwidth=80

" Set spell check region but then spell turn off.
set spell spelllang=en_gb
set nospell

" make spelling errors red and underlined
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
